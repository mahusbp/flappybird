#!/bin/bash

echo "Building..."

if [ ! -d ./build ]; then
    	mkdir ./build
	echo "Created ./build"
fi
if [ -d ./build ]; then 
	cd ./build 
	rm -rf *
	cmake ../
	make
fi
echo "Built in ./build directory"
