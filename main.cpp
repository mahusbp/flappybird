#include "src/Application.h"
#include "src/JSON/json.hpp"
#include "src/JSON/JSParser.hpp"


using json = nlohmann::json;

int main() {
    Application::GI()->start();
    return 0;
}