//
// Created by mhs on 8/5/17.
//

#ifndef FLAPPYBIRD_SCENEMAINMENU_H
#define FLAPPYBIRD_SCENEMAINMENU_H

#include "Scene.h"
#include "LoopTimer.h"
#include "ObjectBird.h"

class SceneMainMenu : public Scene {
public:
    SceneMainMenu();
    virtual ~SceneMainMenu();
    virtual void draw(sf::RenderWindow * window) override;
    virtual void setEvent(sf::Event event) override;
    virtual void update(float delta) override;

private:
    sf::Text *title;
    ObjectBird *bird;
    LoopTimer *timer;
};


#endif //FLAPPYBIRD_MENUMAIN_H
