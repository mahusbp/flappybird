//
// Created by mhs on 8/7/17.
//

#ifndef FLAPPYBIRD_JSPARSER_HPP
#define FLAPPYBIRD_JSPARSER_HPP

#include "json.hpp"
#include <fstream>

using json = nlohmann::json;

class JSParser {
public:
    static JSParser * GI();
    JSParser();
    ~JSParser();
    json& getSettings();
    json& getProfile();
    void saveProfile();
    void saveSettings();
private:
    static JSParser *jsp;
    void init();
    std::string settingsPath;
    std::string profilePath;
    json settings;
    json profile;
};


#endif //FLAPPYBIRD_JSPARSER_HPP
