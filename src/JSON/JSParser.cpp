//
// Created by mhs on 8/7/17.
//

#include <iostream>
#include "JSParser.hpp"

JSParser* JSParser::jsp = nullptr;

JSParser::JSParser() {
    settingsPath = "res/pref/settings.json";
    profilePath = "res/pref/profile.json";
    init();
}

void JSParser::init() {
#ifdef linux
    system("sh src/script/crdir.sh");
#elif _WIN32
    system("sh src/script/crdir.bat"); // TODO windows bat script
#endif
    getProfile();
    getSettings();
    saveProfile();
    saveSettings();
}

JSParser::~JSParser() {
    if (jsp) {
        delete jsp;
        jsp = nullptr;
    }
}

json& JSParser::getSettings() {
    std::ifstream ifs(settingsPath);
    settings = json::parse(ifs);
    ifs.close();
    if (settings["title"].is_null()) settings["title"] = "Birdy Flap!";
    if (settings["resolution"]["width"].is_null()) settings["resolution"]["width"] = 400;
    if (settings["resolution"]["height"].is_null()) settings["resolution"]["height"] = 600;
    if (settings["framelimit"].is_null())   settings["framelimit"]  = 60;
    if (settings["gaccel"].is_null())       settings["gaccel"]      = 6.0;
    if (settings["speed"].is_null())        settings["speed"]       = 100.0;
    if (settings["pi"].is_null())           settings["pi"]          = 3.14159265359;
    if (settings["delta"].is_null())        settings["delta"]       = 1.0 / (int)settings["framelimit"];
    return settings;
}

json& JSParser::getProfile() {
    std::ifstream ifs(profilePath);
    profile = json::parse(ifs);
    ifs.close();

    if (profile["highscore"].is_null()) profile["highscore"] = 0;
    return profile;
}

void JSParser::saveProfile() {
    std::ofstream ofs(profilePath);
    ofs << std::setw(4) << profile << std::endl;
    ofs.close();
}

void JSParser::saveSettings() {
    std::ofstream ofs(settingsPath);
    ofs << std::setw(4) << settings << std::endl;
    ofs.close();
}

JSParser *JSParser::GI() {
    if (!jsp)
        jsp = new JSParser();
    return jsp;
}
