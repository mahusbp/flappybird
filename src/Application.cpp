//
// Created by mhs on 8/6/17.
//

#include "Application.h"
#include "Settings.h"

Application*    Application::manager = nullptr;

Application::Application() {
    srand((unsigned int) time(NULL));
    muzon = new sf::Music();
    if (muzon->openFromFile("res/audio/chipa_0.flac")) {
        muzon->setLoop(true);
        muzon->setVolume(30);
        muzon->play();
    }
}

Application::~Application() {
    if (mainMenu) delete mainMenu;
    if (gameScene) delete gameScene;
}

Application *Application::GI() {
    if (!manager)
        manager = new Application();
    return manager;
}

void Application::start() {
    Settings::init();
    state = MAIN_MENU;
    window = Settings::getWindow();
    window->setPosition(sf::Vector2i(300, 0));
    sf::Event event;
    while (window->isOpen()) {
        switch (state) {
            case MAIN_MENU:
                mainMenuActions();
                break;
            case PAUSE:
                pauseActions();
                break;
            case RESTART:
                restartActions();
                break;
            case RESUME:
                resumeActions();
                break;
            case GAME:
                gameSceneActions();
                break;
            case EXIT:
                exitActions();
                break;
            default:
                break;
        }
        while (window->pollEvent(event)) {
            currentScene->setEvent(event);
        }
    }
    Settings::close();
}

void Application::update(Scene *scene, bool clear) {
    if (clear) window->clear();
    scene->update(Settings::DELTA);
    scene->draw(window);
    window->display();
}

void Application::setState(Application::EState newState) {
    state = newState;
}

void Application::mainMenuActions() {
    if (gameScene) {
        delete gameScene;
        gameScene = nullptr;
    }
    if (pause) {
        delete pause;
        pause = nullptr;
    }
    if (!mainMenu) mainMenu = new SceneMainMenu();
    currentScene = mainMenu;
    update(mainMenu);
}

void Application::gameSceneActions() {
    if (!gameScene) {
        gameScene = new SceneGame();
    }
    if (mainMenu) {
        delete mainMenu;
        mainMenu = nullptr;
    }
    if (pause) {
        delete pause;
        pause = nullptr;
    }
    currentScene = gameScene;
    update(gameScene);
}

void Application::restartActions() {
    if (pause) {
        delete pause;
        pause = nullptr;
    }
    if (gameScene) {
        delete gameScene;
        gameScene = nullptr;
    }
    setState(GAME);
}

void Application::resumeActions() {
    gameScene->pause(false);
    setState(GAME);
}

void Application::pauseActions() {
    if (!pause) pause = new Pause();
    currentScene = pause;
    window->clear();
    gameScene->draw(window);
    gameScene->update(Settings::DELTA);
    update(pause, false);
}

void Application::exitActions() {
    if (mainMenu) delete mainMenu;
    if (gameScene) delete gameScene;
    muzon->stop();
    delete muzon;
    window->close();
}
