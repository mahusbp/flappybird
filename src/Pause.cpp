//
// Created by mhs on 8/6/17.
//

#include "Pause.h"
#include "Application.h"

Pause::Pause() {
    buttons.push_back(new ObjectButton("resume"));
    buttons.push_back(new ObjectButton("restart"));
    buttons.push_back(new ObjectButton("menu"));
    sf::Font * font = new sf::Font();
    if (font->loadFromFile("res/font/GravesideBB.ttf")) {
        title = new sf::Text();
        title->setFont(*font);
        title->setString("Pause...");
        title->setCharacterSize(50);
        title->setColor(sf::Color(90, 20, 180));
        title->setOrigin(title->getGlobalBounds().width/2, title->getGlobalBounds().height/2);
        title->setPosition(Settings::WIDTH/2, 100);
        for (size_t i = 0; i < buttons.size(); i++) {
            sf::Text * text = new sf::Text();
            text->setFont(*font);
            text->setCharacterSize(36);
            text->setColor(sf::Color::Black);
            switch (i) {
                case 0:
                    text->setString("RESUME");
                    break;
                case 1:
                    text->setString("RESTART");
                    break;
                case 2:
                    text->setString("MENU");
                    break;
                default:
                    break;
            }
            buttons[i]->loadTexture("res/img/btn_1.png");
            buttons[i]->setText(*text);
            buttons[i]->setOrigin(Object::Center);
            buttons[i]->setScale(0.3, 0.15);
            buttons[i]->setPosition(Settings::WIDTH/2, 400 + i*(10 + buttons[i]->getBounds().height));
        }
    }
}

void Pause::draw(sf::RenderWindow *window) {
    bg->draw(window);
    for (ObjectButton *b : buttons) {
        b->draw(window);
    }
    window->draw(*title);
}

void Pause::update(float delta) {
    bg->update(delta);
    for (ObjectButton *b : buttons) {
        if (b->mouseHovered() && !b->mousePressed()) {
            b->setScale(0.35f, 0.2f);
        } else if (b->mousePressed()) {
            b->setScale(0.275f, 0.125f);
        } else {
            b->setScale(0.3f, 0.15f);
        }
    }
}

void Pause::setEvent(sf::Event event) {
    if (event.type == sf::Event::Closed)
        Application::GI()->setState(Application::EXIT);

    for (ObjectButton *b : buttons) {
        if (b->mouseReleased()) {
            if (b->getID().toAnsiString() == "resume") {
                Application::GI()->setState(Application::EState::RESUME);
            }
            if (b->getID().toAnsiString() == "restart") {
                Application::GI()->setState(Application::EState::RESTART);
            }
            if (b->getID().toAnsiString() == "menu") {
                Application::GI()->setState(Application::EState::MAIN_MENU);
            }
        }
    }
}

Pause::~Pause() {
    if (title) delete title;
}
