//
// Created by mhs on 8/6/17.
//

#include "ObjectPipe.h"
#include "Settings.h"

ObjectPipe::ObjectPipe() {
    offset = 175;
    minH = 160;
    maxHDiff = 50;
    isPaused = false;
    tUp = new sf::Texture();
    tDn = new sf::Texture();

    if (tDn->loadFromFile("res/img/pipe_d_0.png") && tUp->loadFromFile("res/img/pipe_u_0.png")) {
        int count = (int) ((Settings::WIDTH * 2 / (tDn->getSize().x + offset)) + 1);
        for (size_t i = 0; i < count; ++i) {
            pipes.push_back(new Pipe);
            pipes.back()->scored = false;
            pipes.back()->up.setTexture(*tUp);
            pipes.back()->up.setScale(0.5f, 1);
            pipes.back()->dn.setScale(0.5f, 1);
            pipes.back()->up.setOrigin(0, pipes.back()->up.getGlobalBounds().height);
            pipes.back()->dn.setTexture(*tDn);

            if (i != 0) {
                randomize((int) i);
            }
            else {
                pipes[i]->up.setPosition(Settings::WIDTH + 10, 30 + rand() % 300);
                pipes[i]->dn.setPosition(pipes[i]->up.getPosition().x,
                                         pipes[i]->up.getPosition().y + minH + rand()%(int)maxHDiff-maxHDiff/2);
            }
        }
    }
}

void ObjectPipe::draw(sf::RenderWindow *window) {
    for (auto pipe : pipes) {
        window->draw(pipe->up);
        window->draw(pipe->dn);
    }

//    for (auto p : pipes) {
//        sf::RectangleShape sh1;
//        sf::RectangleShape sh2;
//        sh1.setOutlineColor(sf::Color::Red);
//        sh1.setOutlineThickness(2);
//        sh1.setFillColor(sf::Color(0, 0, 255, 0));
//        sh1.setSize(sf::Vector2f(p->up.getGlobalBounds().width, p->up.getGlobalBounds().height));
//        sh1.setPosition(p->up.getGlobalBounds().left, p->up.getGlobalBounds().top);
//        sh2.setOutlineColor(sf::Color::Blue);
//        sh2.setOutlineThickness(2);
//        sh2.setFillColor(sf::Color(0, 0, 255, 0));
//        sh2.setSize(sf::Vector2f(p->dn.getGlobalBounds().width, p->dn.getGlobalBounds().height));
//        sh2.setPosition(p->dn.getGlobalBounds().left, p->dn.getGlobalBounds().top);
//
//        window->draw(sh1);
//        window->draw(sh2);
//    }
}

void ObjectPipe::update(float delta) {
    if (isPaused) return;
    for (size_t i = 0; i < pipes.size(); ++i) {
        pipes[i]->dn.setPosition(pipes[i]->dn.getPosition().x - Settings::SPEED * delta, pipes[i]->dn.getPosition().y);
        pipes[i]->up.setPosition(pipes[i]->up.getPosition().x - Settings::SPEED * delta, pipes[i]->up.getPosition().y);
        if (pipes[i]->up.getPosition().x + pipes[i]->up.getGlobalBounds().width < 0) {
            randomize((int) i);
        }
    }
}


ObjectPipe::~ObjectPipe() {
    for (auto itr = pipes.begin(); itr != pipes.end();) {
        if (*itr) {
            delete *itr;
            pipes.erase(itr);
        } else itr++;
    }
    if (tUp) delete tUp;
    if (tDn) delete tDn;
}

void ObjectPipe::randomize(int pNum) {
    if (pipes[pNum]) {
        pipes[pNum]->scored = false;
        sf::Vector2f up = {pipes[pNum]->up.getPosition()};
        sf::Vector2f dn = {pipes[pNum]->dn.getPosition()};
        sf::Vector2f upl;
        if (pNum != 0)
            upl = pipes[pNum-1]->up.getPosition();
        else upl = pipes.back()->up.getPosition();
        up.x = upl.x + offset + rand() % 25;
        up.y = upl.y + rand() % 200 - 100;
        if (up.y < 30) up.y = 30;
        if (up.y > Settings::HEIGHT - 120 - (minH + maxHDiff)) up.y = Settings::HEIGHT - 120 - (minH + maxHDiff);
        dn.x = up.x;
        dn.y = up.y + minH + rand()%(int)maxHDiff-maxHDiff/2;
        pipes[pNum]->up.setPosition(up.x, up.y);
        pipes[pNum]->dn.setPosition(dn.x, dn.y);
    }
}

float ObjectPipe::getPosition(int pNum) {
    if (pipes[pNum]) {
        return pipes[pNum]->dn.getPosition().x;
    }
    return INFINITY;
}

int ObjectPipe::score(int pNum) {
    if (!pipes[pNum]->scored) {
        pipes[pNum]->scored = true;
        return 1;
    }
    return 0;
}

unsigned ObjectPipe::getPipeCount() {
    return (unsigned int) pipes.size();
}

void ObjectPipe::pause(bool active) {
    isPaused = active;
}

bool ObjectPipe::intersects(int pNum, sf::FloatRect rect) {
    if (pipes[pNum]->up.getGlobalBounds().intersects(rect)) return true;
    return pipes[pNum]->dn.getGlobalBounds().intersects(rect);
}
