//

// Created by mhs on 8/5/17.

#include "Object.h"

Object::Object() {
    simpleRect = new sf::FloatRect();
    texture = new sf::Texture();
    sprite = new sf::Sprite();
    isPaused = false;
}

void Object::draw(sf::RenderWindow *window) {
    if (sprite) {
        window->draw(*sprite);
//        sf::RectangleShape sh1;
//        sh1.setOutlineColor(sf::Color::Black);
//        sh1.setOutlineThickness(1);
//        sh1.setFillColor(sf::Color(0,0,255,0));
//        sh1.setSize(sf::Vector2f(simpleRect->width, simpleRect->height));
//        sh1.setPosition(simpleRect->left, simpleRect->top);
//        sh1.setSize(sf::Vector2f(getBounds().left, getBounds().top));
//        sh1.setPosition(simpleRect->left, simpleRect->height);
//        window->draw(sh1);
    }
}

void Object::setPosition(float x, float y) {
    if (sprite) {
        sprite->setPosition(x, y);
    }
}

sf::FloatRect Object::getBounds() {
    if (sprite) {
        return sprite->getGlobalBounds();
    }
    return sf::FloatRect();
}

void Object::setOrigin(float x, float y) {
    if (sprite) {
        sprite->setOrigin(x, y);
    }
}

void Object::setOrigin(Object::Point point) {
    switch (point) {
        case TopLeft:
            setOrigin(0,0);
            break;
        case TopRight:
            setOrigin(sprite->getGlobalBounds().width, 0);
            break;
        case BottomLeft:
            setOrigin(0, sprite->getGlobalBounds().height);
            break;
        case BottomRight:
            setOrigin(sprite->getGlobalBounds().width, sprite->getGlobalBounds().height);
            break;
        case Center:
            setOrigin(sprite->getGlobalBounds().width/2, sprite->getGlobalBounds().height/2);
            break;
        default:
            break;
    }
}

void Object::setScale(float x, float y) {
    if (sprite) {
        sprite->setScale(x, y);
        simpleRect->height  = sprite->getScale().y * sprite->getLocalBounds().height;
        simpleRect->width   = sprite->getScale().x * sprite->getLocalBounds().width;
    }
}

void Object::loadTexture(sf::String imgPath) {
    if (sprite) {
        if (texture->loadFromFile(imgPath))
            sprite->setTexture(*this->texture);
    }
}

Object::~Object() {
    if (sprite) {
        delete sprite;
        sprite = nullptr;
    }
    if (texture) {
        delete texture;
        texture = nullptr;
    }
}

sf::Texture &Object::getTexture() const {
    return *texture;
}

void Object::pause(bool stop) {
    isPaused = stop;
}

sf::Vector2f Object::getScale() const {
    return sprite->getScale();
}

sf::FloatRect& Object::getRect() const {
    return *simpleRect;
}
