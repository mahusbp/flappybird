//
// Created by mhs on 8/5/17.
//

#ifndef FLAPPYBIRD_SCENE_H
#define FLAPPYBIRD_SCENE_H

#include "ObjectBackground.h"
#include "ObjectButton.h"


class Scene {
public:
    Scene();
    virtual ~Scene();
    virtual void draw(sf::RenderWindow * window) = 0;
    virtual void setEvent(sf::Event event) = 0;
    virtual void update(float delta) = 0;

protected:
    ObjectBackground * bg;
    std::vector<ObjectButton*> buttons;
};


#endif //FLAPPYBIRD_SCENE_H
