//
// Created by mhs on 8/6/17.
//

#ifndef FLAPPYBIRD_TIMER_H
#define FLAPPYBIRD_TIMER_H

class LoopTimer {
public:
    LoopTimer(float begin, float end);
    virtual ~LoopTimer() {};
    virtual void update(float delta);
    virtual float getValue();
private:
    float mValue;
    float mBegin;
    float mEnd;
    bool isGrowing;
};

#endif //FLAPPYBIRD_TIMER_H
