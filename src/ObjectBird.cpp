//
// Created by mhs on 8/4/17.
//

#include <iostream>
#include "ObjectBird.h"
#include "Settings.h"

ObjectBird::ObjectBird() {
    mass = 250;
    velocity  = {0, -1};
    alive   = true;
    godMode = false;

    timer = new LoopTimer(-2 * Settings::PI, 2 * Settings::PI);
    if (texture->loadFromFile("res/img/bird_0.png")) {
        sprite->setTexture(*texture);
    }
}

void ObjectBird::update(float delta) {
    if (isPaused) {
        timer->update(delta * 5);
        sprite->setPosition(sprite->getPosition().x,
                            sprite->getPosition().y + (float) sin(timer->getValue() * 2 * delta));
        return;
    };
    if (alive) {
        sprite->setPosition(sprite->getPosition().x, sprite->getPosition().y + velocity.y * mass * delta);
        simpleRect->left    = getBounds().left;
        simpleRect->top     = getBounds().top;

        if (sprite->getPosition().y > Settings::HEIGHT - 75 || sprite->getPosition().y < getBounds().height/2) {
            stun();
        }
    } else {
        sprite->setPosition(sprite->getPosition().x + velocity.x * mass * delta,
                            sprite->getPosition().y + velocity.y * mass * delta);
    }
    sprite->setRotation(2.5f * velocity.y * mass * delta);
    velocity.y += delta * Settings::GACCEL;
}

void ObjectBird::jump() {
    if (alive) {
        velocity.y = -1.8f;
    }
}

ObjectBird::~ObjectBird() {
    if (timer) delete timer;
}

bool ObjectBird::isAlive() {
    return alive;
}

void ObjectBird::stun() {
    if (!godMode) {
        alive = false;
        velocity = {-1, -1};
    }
}

