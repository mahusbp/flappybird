//
// Created by mhs on 8/6/17.
//

#ifndef FLAPPYBIRD_PAUSE_H
#define FLAPPYBIRD_PAUSE_H

#include "Scene.h"
#include "Settings.h"

class Pause : public Scene {
public:
    Pause();
    virtual ~Pause();
    virtual void draw(sf::RenderWindow * window) override;
    virtual void setEvent(sf::Event event) override;
    virtual void update(float delta) override;

private:
    sf::Text *highscore;
    sf::Text *title;
};


#endif //FLAPPYBIRD_PAUSE_H
