//
// Created by mhs on 8/6/17.
//

#ifndef FLAPPYBIRD_MENUGAME_H
#define FLAPPYBIRD_MENUGAME_H

#include "ObjectBird.h"
#include "ObjectBackground.h"
#include "Scene.h"
#include "ObjectPipe.h"

class SceneGame : public Scene {
public:
    SceneGame();
    virtual ~SceneGame();
    virtual void draw(sf::RenderWindow * window) override;
    virtual void setEvent(sf::Event event) override;
    virtual void update(float delta) override;
    virtual void pause(bool active);
private:
    bool gameStarted;
    bool gamePaused;
    float ggTimer;
    unsigned score;
    sf::Text * text;
    sf::Text * scoreText;
    sf::Text * hScoreText;
    ObjectBackground * fg;
    ObjectBird * bird;
    ObjectPipe * pipes;
};


#endif //FLAPPYBIRD_MENUGAME_H
