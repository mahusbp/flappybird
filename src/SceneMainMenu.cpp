//
// Created by mhs on 8/5/17.
//

#include "SceneMainMenu.h"
#include "Settings.h"
#include "Application.h"


SceneMainMenu::SceneMainMenu() {
    buttons.push_back(new ObjectButton("play"));
    buttons.push_back(new ObjectButton("exit"));
    sf::Font * font = new sf::Font();
    title = new sf::Text();
    if (font->loadFromFile("res/font/GravesideBB.ttf")) {
        title->setFont(*font);
        title->setString("Birdy Flap!");
        title->setCharacterSize(50);
        title->setColor(sf::Color(90, 20, 180));
        title->setOrigin(title->getGlobalBounds().width/2, title->getGlobalBounds().height/2);
        title->setPosition(Settings::WIDTH/2, 100);
        for (size_t i = 0; i < buttons.size(); i++) {
            sf::Text * text = new sf::Text();
            text->setFont(*font);
            text->setCharacterSize(36);
            text->setColor(sf::Color::Black);
            if (i == 0) text->setString("PLAY");
            if (i == 1) text->setString("EXIT");
            buttons[i]->loadTexture("res/img/btn_1.png");
            buttons[i]->setText(*text);
            buttons[i]->setOrigin(Object::Center);
            buttons[i]->setScale(0.3, 0.15);
            buttons[i]->setPosition(Settings::WIDTH/2, 400 + i*(10 + buttons[i]->getBounds().height));
        }
    }
    bg->setSpeed(-10, 0);
    bg->loadTexture("res/img/bg.png");
    bg->setScale((float)Settings::WIDTH / bg->getTexture().getSize().x,
                 (float)Settings::HEIGHT / bg->getTexture().getSize().y);

    bird = new ObjectBird();
    bird->setOrigin(Object::Center);
    bird->setScale(0.5, 0.5);
    bird->setPosition(Settings::WIDTH/2, 250);
    timer = new LoopTimer(-2 * Settings::PI, 2 * Settings::PI);

}


void SceneMainMenu::draw(sf::RenderWindow *window) {
    bg->draw(window);
    for (ObjectButton *b : buttons) {
        b->draw(window);
    }
    bird->draw(window);
    window->draw(*title);
}

void SceneMainMenu::update(float delta) {
    bg->update(delta);
    timer->update(delta);
    bird->setPosition(Settings::WIDTH/2 + (float)cos(timer->getValue() * 50 * delta) * 15,
                      250 + (float)sin(timer->getValue() * 200 * delta) * 15);

    for (ObjectButton *b : buttons) {
        if (b->mouseReleased()) {
            if (b->getID().toAnsiString() == "play") {
                Application::GI()->setState(Application::EState::GAME);
            }
            if (b->getID().toAnsiString() == "exit") {
                Application::GI()->setState(Application::EState::EXIT);
            }
        }
    }
}

void SceneMainMenu::setEvent(sf::Event event) {
    if (event.type == sf::Event::Closed)
        Application::GI()->setState(Application::EXIT);
    for (ObjectButton *b : buttons) {
        if (b->mouseHovered() && !b->mousePressed()) {
            b->setScale(0.31f, 0.16f);
        } else if (b->mousePressed()) {
            b->setScale(0.29f, 0.14f);
        } else {
            b->setScale(0.3f, 0.15f);
        }
    }
}

SceneMainMenu::~SceneMainMenu() {
    if (bird)
        delete bird;
    if (timer)
        delete timer;
}
