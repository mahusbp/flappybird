//
// Created by mhs on 8/5/17.
//

#ifndef FLAPPYBIRD_OBJECTBACKGROUND_H
#define FLAPPYBIRD_OBJECTBACKGROUND_H


#include "Object.h"

class ObjectBackground : public Object {
public:
    ObjectBackground();
    virtual ~ObjectBackground();
    virtual void loadTexture(sf::String imgPath) override;
    virtual void draw(sf::RenderWindow * window) override;
    virtual void setPosition(float x, float y) override;
    virtual void setScale(float x, float y) override;
    virtual void update(float delta) override;
    virtual void setSpeed(float x, float y);
private:
    void sortSprites();
    sf::Vector2f bgSpeed;
    std::vector<sf::Sprite*> bg;
};


#endif //FLAPPYBIRD_OBJECTBACKGROUND_H
