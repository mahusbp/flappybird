//
// Created by mhs on 8/6/17.
//

#include "LoopTimer.h"

LoopTimer::LoopTimer(float begin, float end) {
    mValue = begin;
    mBegin = begin;
    mEnd = end;
    isGrowing = true;
}
void LoopTimer::update(float delta) {
    if (isGrowing) {
        mValue += delta;
        if (mValue > mEnd) isGrowing = false;
    } else if (!isGrowing) {
        mValue -= delta;
        if (mValue < mBegin) isGrowing = true;
    }
}
float LoopTimer::getValue() {
    return mValue;
}
