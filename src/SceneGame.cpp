//
// Created by mhs on 8/6/17.
//

#include "SceneGame.h"
#include "Settings.h"
#include "Application.h"

SceneGame::SceneGame() {
    score = 0;
    ggTimer = 2.0f;
    gamePaused = false;
    gameStarted = false;

    bird = new ObjectBird();
    bird->setOrigin(Object::Center);
    bird->setScale(0.5, 0.5);
    bird->setPosition(75, 300);

    bg->setSpeed(-10, 0);
    bg->loadTexture("res/img/bg.png");
    bg->setScale((float)Settings::WIDTH / bg->getTexture().getSize().x,
                 (float)Settings::HEIGHT / bg->getTexture().getSize().y);

    fg = new ObjectBackground();
    fg->setSpeed(-Settings::SPEED, 0);
    fg->loadTexture("res/img/fg.png");
    fg->setScale((float)Settings::WIDTH / fg->getTexture().getSize().x, 1);
    fg->setPosition(0, Settings::HEIGHT - fg->getTexture().getSize().y);

    sf::Font * font = new sf::Font();
    if (font->loadFromFile("res/font/GravesideBB.ttf")) {
        text = new sf::Text("Press any key", *font, 36);
        text->setColor(sf::Color::Black);
        text->setOrigin(text->getGlobalBounds().width / 2, text->getGlobalBounds().height / 2);
        text->setPosition(Settings::WIDTH / 2, 200);
        scoreText = new sf::Text("SCORE: " + std::to_string(score), *font, 30);
        scoreText->setColor(sf::Color::Black);
        scoreText->setPosition(20, Settings::HEIGHT - 50);

        hScoreText = new sf::Text();
        hScoreText->setFont(*font);
        hScoreText->setString("HIGHSCORE: " + std::to_string((int)JSParser::GI()->getProfile()["highscore"]));
        hScoreText->setCharacterSize(25);
        hScoreText->setColor(sf::Color(sf::Color::Black));
        hScoreText->setPosition(20, Settings::HEIGHT - 75);
    }

    buttons.push_back(new ObjectButton("pause"));
    buttons.back()->loadTexture("res/img/pause.png");
    buttons.back()->setOrigin(Object::Center);
    buttons.back()->setPosition(Settings::WIDTH - 50, Settings::HEIGHT - 50);

    pipes = new ObjectPipe();
    bird->pause(true);
}

SceneGame::~SceneGame() {
    if (scoreText) delete scoreText;
    if (hScoreText) delete hScoreText;
    if (pipes) delete pipes;
    if (bird) delete bird;
    if (text) delete text;
    if (fg) delete fg;
}

void SceneGame::draw(sf::RenderWindow *window) {
    bg->draw(window);
    pipes->draw(window);
    fg->draw(window);
    bird->draw(window);
    if (!gameStarted) {
        window->draw(*text);
    }
    if (gameStarted) {
        window->draw(*hScoreText);
        window->draw(*scoreText);
        for (ObjectButton *b : buttons) {
            b->draw(window);
        }
    }

}

void SceneGame::update(float delta) {
    if (!gamePaused) {
        if (gameStarted) {
            pipes->update(delta);
            if (!bird->isAlive()) {
                if (ggTimer < 0) Application::GI()->setState(Application::MAIN_MENU);
                ggTimer -= delta;
            }
            if (bird->isAlive()) {
                for (int i = 0; i < pipes->getPipeCount(); ++i) {
                    if (pipes->getPosition(i) < bird->getBounds().left) {
                        score += pipes->score(i);
                        scoreText->setString("SCORE: " + std::to_string(score));
                        if (JSParser::GI()->getProfile()["highscore"] < score) {
                            JSParser::GI()->getProfile()["highscore"] = score;
                            hScoreText->setString("HIGHSCORE: " + std::to_string(score));
                        }
                    }
                    if (pipes->intersects(i, bird->getRect())) {
                        JSParser::GI()->saveProfile();

                        bird->stun();
                    }
                }
                for (ObjectButton *b : buttons) {
                    if (b->mouseHovered() && !b->mousePressed()) {
                        b->setScale(0.26f, 0.26f);
                    } else if (b->mousePressed()) {
                        b->setScale(0.24f, 0.24f);
                    } else {
                        b->setScale(0.25f, 0.25f);
                    }
                }
            }
        }
        bg->update(delta);
        fg->update(delta);
        bird->update(delta);
    }
}

void SceneGame::setEvent(sf::Event event) {
    if (event.type == sf::Event::Closed)
        Application::GI()->setState(Application::EXIT);
    if (event.type == sf::Event::KeyPressed && !gameStarted) {
        gameStarted = true;
        bird->pause(false);
    }
    if (event.type == sf::Event::KeyPressed && gameStarted && !gamePaused) {
        if (event.key.code == sf::Keyboard::Up) {
            bird->jump();
        }
    }
    for (ObjectButton *b : buttons) {
        if (b->mouseReleased()) {
            if (b->getID().toAnsiString() == "pause") {
                Application::GI()->setState(Application::EState::PAUSE);
                pause(true);
            }
        }
    }
}

void SceneGame::pause(bool active) {
    if (active) {
        bird->pause(true);
        bg->pause(true);
        fg->pause(true);
        pipes->pause(true);
        gamePaused = true;
    } else {
        bird->pause(false);
        bg->pause(false);
        fg->pause(false);
        pipes->pause(false);
        gamePaused = false;
    }
}
