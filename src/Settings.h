//
// Created by mhs on 8/5/17.
//

#ifndef FLAPPYBIRD_SETTINGS_H
#define FLAPPYBIRD_SETTINGS_H

#include "SFML/Graphics.hpp"
#include <string>
#include "JSON/JSParser.hpp"

class Settings {
public:
    static unsigned WIDTH;
    static unsigned HEIGHT;
    static unsigned FRAMELIMIT;
    static float GACCEL;
    static float SPEED;
    static float DELTA;
    static float PI;
    static std::string TITLE;

    static void init();
    static void close();
    static sf::RenderWindow * getWindow();

private:
    static sf::RenderWindow * window;
};

#endif //FLAPPYBIRD_SETTINGS_H
