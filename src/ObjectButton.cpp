//
// Created by mhs on 8/5/17.
//

#include "ObjectButton.h"
#include "Settings.h"

ObjectButton::ObjectButton(sf::String uID) {
    wasPressed = false;
    text = new sf::Text();
    id = uID;
}

void ObjectButton::draw(sf::RenderWindow *window) {
    window->draw(*sprite);
    window->draw(*text);
}

void ObjectButton::update(float delta) {

}

bool ObjectButton::mousePressed() {
    if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
        if (mouseHovered()) {
            wasPressed = true;
            return true;
        }
    }
    return false;
}

bool ObjectButton::mouseReleased() {
    if (!mouseHovered() && !mousePressed())
        wasPressed = false;

    if (mouseHovered()) {
        if (wasPressed && !mousePressed()) {
            wasPressed = false;
            return true;
        }
    }
    return false;
}

bool ObjectButton::mouseHovered() {
    return sprite->getGlobalBounds().contains(sf::Mouse::getPosition(*Settings::getWindow()).x,
                                              sf::Mouse::getPosition(*Settings::getWindow()).y);
}

void ObjectButton::setPosition(float x, float y) {
    sprite->setPosition(x, y);
    setTextPosition();
}

void ObjectButton::setText(sf::Text &txt) {
    text = &txt;
    setTextPosition();
}

void ObjectButton::setOrigin(float x, float y) {
    sprite->setOrigin(x, y);
    setTextPosition();
}

void ObjectButton::setOrigin(Object::Point point) {
    switch (point) {
        case TopLeft:
            setOrigin(0,0);
            break;
        case TopRight:
            setOrigin(sprite->getGlobalBounds().width, 0);
            break;
        case BottomLeft:
            setOrigin(0, sprite->getGlobalBounds().height);
            break;
        case BottomRight:
            setOrigin(sprite->getGlobalBounds().width, sprite->getGlobalBounds().height);
            break;
        case Center:
            setOrigin(sprite->getGlobalBounds().width/2, sprite->getGlobalBounds().height/2);
            break;
        default:
            break;
    }
}

void ObjectButton::setString(sf::String str) {
    text->setString(str);
    setTextPosition();
}

ObjectButton::~ObjectButton() {
    if (text) {
        delete text;
        text = nullptr;
    }
}

void ObjectButton::setScale(float x, float y, bool scaleText) {
    sprite->setScale(x, y);
    if (scaleText)
        text->setScale(x, y);
    setTextPosition();
}

void ObjectButton::setTextPosition() {
    text->setOrigin(text->getLocalBounds().width/2, text->getLocalBounds().height/2);
    float x = sprite->getPosition().x;
    float y = sprite->getPosition().y - text->getGlobalBounds().height/2;
    text->setPosition(x, y);
}

sf::String ObjectButton::getID() {
    return id;
}

