//
// Created by mhs on 8/5/17.
//

#include "Scene.h"

Scene::Scene() {
    bg = new ObjectBackground();
}

Scene::~Scene() {
    for (auto itr = buttons.begin(); itr != buttons.end();) {
        if (*itr) {
            delete (*itr);
            buttons.erase(itr);
        }
    }

    if (bg) {
        delete bg;
        bg = nullptr;
    }
}
