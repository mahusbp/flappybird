//
// Created by mhs on 8/4/17.
//

#ifndef FLAPPYBIRD_BIRD_H
#define FLAPPYBIRD_BIRD_H

#include "Object.h"
#include "LoopTimer.h"

class ObjectBird : public Object {
public:
    ObjectBird();
    virtual ~ObjectBird();
    virtual void update(float delta) override;
    virtual void stun();
    virtual void jump();
    virtual bool isAlive();
private:
    sf::Vector2f velocity;
    LoopTimer * timer;
    float mass;
    bool godMode;
    bool alive;
};


#endif //FLAPPYBIRD_BIRD_H
