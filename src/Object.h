//
// Created by mhs on 8/5/17.
//

#ifndef FLAPPYBIRD_OBJECT_H
#define FLAPPYBIRD_OBJECT_H


#include <SFML/Graphics.hpp>


class Object {
public:
    enum Point {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        Center,
    };
    Object();
    virtual ~Object();
    virtual void draw(sf::RenderWindow * window);
    virtual void update(float delta) = 0;
    virtual void loadTexture(sf::String imgPath);
    virtual void setPosition(float x, float y);
    virtual void setOrigin(float x, float y);
    virtual void setOrigin(Point point);
    virtual void setScale(float x, float y);
    virtual void pause(bool stop);
    virtual sf::Texture & getTexture() const;
    virtual sf::FloatRect& getRect() const;
    virtual sf::Vector2f getScale() const;
    sf::FloatRect getBounds();
protected:
    std::vector<sf::FloatRect*> complexRect;
    sf::FloatRect * simpleRect;
    sf::Texture * texture;
    sf::Sprite * sprite;
    bool isPaused;
};


#endif //FLAPPYBIRD_OBJECT_H
