//
// Created by mhs on 8/6/17.
//

#ifndef FLAPPYBIRD_OBJECTPIPE_H
#define FLAPPYBIRD_OBJECTPIPE_H

#include <SFML/Graphics.hpp>

class ObjectPipe {
public:
    ObjectPipe();
    virtual ~ObjectPipe();
    virtual void draw(sf::RenderWindow * window);
    virtual void update(float delta);
    virtual void pause(bool active);
    virtual int score(int pNum);
    virtual unsigned getPipeCount();
    virtual float getPosition(int pNum);
    virtual bool intersects(int pNum, sf::FloatRect rect);
private:
    bool isPaused;
    float offset;
    float minH;
    float maxHDiff;
    struct Pipe {
        sf::Sprite up;
        sf::Sprite dn;
        bool scored;
    };
    sf::Texture *tUp;
    sf::Texture *tDn;
    std::vector<Pipe*> pipes;
    void randomize(int pNum);
};


#endif //FLAPPYBIRD_OBJECTPIPE_H
