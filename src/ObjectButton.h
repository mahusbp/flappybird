//
// Created by mhs on 8/5/17.
//

#ifndef FLAPPYBIRD_OBJECTBUTTON_H
#define FLAPPYBIRD_OBJECTBUTTON_H


#include "Object.h"

class ObjectButton : public Object {
public:
    ObjectButton(sf::String uID = "");
    virtual ~ObjectButton();
    virtual void draw(sf::RenderWindow * window) override;
    virtual void setPosition(float x, float y) override;
    virtual void setOrigin(float x, float y) override;
    virtual void setScale(float x, float y, bool scaleText = false);
    virtual void setOrigin(Point point) override;
    virtual void update(float delta) override;
    virtual void setString(sf::String str);
    virtual void setText(sf::Text & txt);
    virtual bool mousePressed();
    virtual bool mouseReleased();
    virtual bool mouseHovered();
    virtual sf::String getID();

protected:
    sf::String id;
    sf::Text * text;
    bool wasPressed;

private:
    void setTextPosition();
};


#endif //FLAPPYBIRD_OBJECTBUTTON_H
