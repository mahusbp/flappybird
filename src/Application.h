//
// Created by mhs on 8/6/17.
//

#ifndef FLAPPYBIRD_APPLICATION_H
#define FLAPPYBIRD_APPLICATION_H

#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "SceneMainMenu.h"
#include "SceneGame.h"
#include "Pause.h"

class Application {
public:
    enum EState {
        MAIN_MENU,
        RESTART,
        RESUME,
        PAUSE,
        GAME,
        EXIT,
    };
    Application();
    virtual ~Application();
    virtual void start();
    virtual void setState(EState newState);
    static Application* GI();
private:
    static Application * manager;
    sf::Music * muzon;
    sf::RenderWindow * window;
    SceneMainMenu * mainMenu;
    SceneGame * gameScene;
    Scene * currentScene;
    Pause * pause;
    EState state;
    void update(Scene *scene, bool clear = true);
    void gameSceneActions();
    void mainMenuActions();
    void restartActions();
    void resumeActions();
    void pauseActions();
    void exitActions();
};


#endif //FLAPPYBIRD_APPLICATION_H
