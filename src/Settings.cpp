//
// Created by mhs on 8/5/17.
//
#include "Settings.h"

sf::RenderWindow *  Settings::window        = nullptr;
std::string         Settings::TITLE         = "";
unsigned            Settings::WIDTH         = 0;
unsigned            Settings::HEIGHT        = 0;
unsigned            Settings::FRAMELIMIT    = 0;
float               Settings::GACCEL        = 0;
float               Settings::SPEED         = 0;
float               Settings::DELTA         = 0;
float               Settings::PI            = 0;

void Settings::init() {
    json j = JSParser::GI()->getSettings();
    TITLE       = j["title"];
    FRAMELIMIT  = j["framelimit"];
    HEIGHT      = j["resolution"]["height"];
    WIDTH       = j["resolution"]["width"];
    GACCEL      = j["gaccel"];
    SPEED       = j["speed"];
    DELTA       = j["delta"];
    PI          = j["pi"];

    window = new sf::RenderWindow(sf::VideoMode(WIDTH, HEIGHT), TITLE);
    window->setFramerateLimit(FRAMELIMIT);
    window->setKeyRepeatEnabled(false);
}

void Settings::close() {
    if (window) {
        delete window;
        window = nullptr;
    }
};

sf::RenderWindow *Settings::getWindow() {
    if (!window)
        init();
    return window;
}

