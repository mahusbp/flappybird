//
// Created by mhs on 8/5/17.
//

#include "ObjectBackground.h"
#include "Settings.h"

ObjectBackground::ObjectBackground() {
    bgSpeed = {0, 0};
    texture = new sf::Texture();
}

void ObjectBackground::loadTexture(sf::String imgPath) {
    if (texture->loadFromFile(imgPath)) {
        int count = (Settings::WIDTH * 2 / texture->getSize().x) + 1;
        for (size_t i = 0; i < count; ++i) {
            bg.push_back(new sf::Sprite(*texture));
        }
        sortSprites();
    }
}

void ObjectBackground::setScale(float x, float y) {
    for (size_t i = 0; i < bg.size(); ++i) {
        bg[i]->setScale(x, y);
    }
    sortSprites();
}

void ObjectBackground::setSpeed(float x, float y) {
    bgSpeed.x = x;
    bgSpeed.y = y;
}

void ObjectBackground::setPosition(float x, float y) {
    bg[0]->setPosition(x, y);
    for (size_t i = 1; i < bg.size(); ++i) {
        bg[i]->setPosition(bg[i-1]->getPosition().x + bg[i-1]->getLocalBounds().width, y);
    }
}

void ObjectBackground::draw(sf::RenderWindow *window) {
    for (auto itr = bg.begin(); itr != bg.end(); itr++) {
        window->draw(*(*itr));
    }
}

void ObjectBackground::sortSprites() {
    for (size_t i = 1; i < bg.size(); ++i) {
        bg[i]->setPosition(bg[i-1]->getPosition().x + bg[i-1]->getLocalBounds().width - 5,
                           bg[i-1]->getPosition().y);
    }
}

void ObjectBackground::update(float delta) {
    if (isPaused) return;
    for (size_t i = 0; i < bg.size(); ++i) {
        bg[i]->setPosition(bg[i]->getPosition() + bgSpeed * delta);
        if (bg[i]->getPosition().x + bg[i]->getLocalBounds().width < 0) {
            if (i != 0) {
                bg[i]->setPosition(bg[i - 1]->getPosition().x + bg[i]->getLocalBounds().width - 5,
                                   bg[i - 1]->getPosition().y);
            } else {
                bg[i]->setPosition(bg.back()->getPosition().x + bg.back()->getLocalBounds().width - 5,
                                   bg.back()->getPosition().y);
            }
        }
    }
}

ObjectBackground::~ObjectBackground() {
    for (auto itr = bg.begin(); itr != bg.end();) {
        if (*itr) {
            delete (*itr);
            bg.erase(itr);
        }
    }
    if (texture) {
        delete texture;
        texture = nullptr;
    }
}
