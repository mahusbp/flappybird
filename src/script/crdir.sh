#!/bin/bash

pref="res/pref"

if [ ! -d $pref ]; then
	mkdir $pref
fi
if [ ! -f $pref/profile.json ]; then
	echo "{}" > $pref/profile.json
fi
if [ ! -f $pref/settings.json ]; then
	echo "{}" > $pref/settings.json
fi